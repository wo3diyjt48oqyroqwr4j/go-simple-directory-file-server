package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)


func main() {
	if len(os.Args) < 2  {
		fmt.Printf("Usage: %s path\n", os.Args[0])
		return
	}
	if _, err := os.Stat(os.Args[1]); os.IsNotExist(err) {
		log.Println("path not found:", os.Args[1])
		return
	}
	http.Handle("/", http.FileServer(http.Dir(os.Args[1])))
	if err := http.ListenAndServe(":8095", nil); err != nil {
		return
	}
}
